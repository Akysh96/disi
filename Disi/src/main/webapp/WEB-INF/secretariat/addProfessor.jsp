<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <spring:url value="/resources/css/global.css" var="mainCss"/>
    <jsp:include page="/WEB-INF/login/secretariat.jsp"></jsp:include>
    <link href="${mainCss}" rel="stylesheet"/>
    <title>Update</title>
    <style>
        h4 {
            text-align: center;
            color: red;
        }

        h5 {
            color: red;
        }
    </style>
</head>
<body class="bg" style="min-width: 960px; margin: 0 auto;">
<%--@elvariable id="user" type="model"--%>
<%--<h4><spring:message code="label.submit"/></h4>--%>
<h4>Add Professor</h4>
<div class="container-fluid ">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xm-12">
        </div>

        <div class="col-md-4 col-sm-4 col-xm-12">
            <form action="/addProfessor" method="post">
                <table align="center">
                    <tr>
                        <div class="form-group">
                            <td><h5>Nume : </h5></td>
                            <td><input class="form-control" type="text" name="nume" required="required"
                                       placeholder="Enter name"/></td>
                        </div>
                    </tr>
                    <tr>
                        <div class="form-group">
                            <td><h5>Prenume : </h5></td>
                            <td><input class="form-control" type="text" name="prenume" required="required"
                                       placeholder="Enter surname"/></td>
                        </div>
                    </tr>
                    <tr>
                        <div class="form-group">
                            <td><h5>Materia : </h5></td>
                            <td>
                                <select name="materia" required>
                                    <c:forEach items="${materie}" var="met">
                                        <option value="${met.id_materie}">
                                                ${met.denumire}
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>
                        </div>
                    </tr>

                    </tr>
                    <td align="right">
                        <button id="login" class="btn btn-primary" name="updateInfo"> Add</button>
                    </td>

                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<table align="center">
    <tr>
        <td style="font-style: italic; color: red;">${message}</td>
    </tr>
</table>
</body>
</html>