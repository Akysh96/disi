<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <spring:url value="/resources/css/student.css" var="mainCss"/>
    <link href="${mainCss}" rel="stylesheet"/>
    <title>Login</title>
    <style>
        h4 {
            text-align: center;
            color: red;
        }
        h5 {
            color: red;
        }
    </style>
</head>
<body class="bg" style="min-width: 960px; margin: 0 auto;">
<%--@elvariable id="user" type="model"--%>
<%--<h4><spring:message code="label.submit"/></h4>--%>
<h4>LOGIN</h4>
<div class="container-fluid ">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xm-12">
        </div>

        <div class="col-md-4 col-sm-4 col-xm-12">
            <form  action="/loginProcess" method="post">
                <table align="center">
                    <tr>
                        <div class="form-group">
                            <td ><h5>Username : </h5> </td>
                            <td><input class="form-control" type="text" name="username" required="required"
                                       placeholder="Enter username"/></td>
                        </div>
                    </tr>

                    </tr>
                    <tr>
                        <div class="form-group">

                            <td ><h5>Password : </h5> </td>
                            <td><input class="form-control" type="password" name="password" required="required"
                                       placeholder="Enter password"/></td>
                        </div>
                    <tr>
                        <td align="left"><a class="btn btn-primary" href="/logout">Home</a>
                        </td>
                        <td align="right">
                            <button id="login" class="btn btn-primary" name="login"> Submit </button>
                        </td>

                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<table align="center">
    <tr>
        <td style="font-style: italic; color: red;">${message}</td>
    </tr>
</table>
</body>
</html>