<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<jsp:include page="/WEB-INF/login/profesor.jsp"></jsp:include>
<h4>Note List</h4>
<head>
    <script type="text/javascript">
        function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <style>
        #myInput {
            background-position: 10px 12px; /* Position the search icon */
            background-repeat: no-repeat; /* Do not repeat the icon image */
            width: 100%; /* Full-width */
            font-size: 16px; /* Increase font-size */
            padding: 12px 10px 12px 40px; /* Add some padding */
            border: 1px solid #ddd; /* Add a grey border */
            margin-bottom: 12px; /* Add some space below the input */
        }
    </style>
</head>
<div class="container-fluid ">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xm-12"></div>
        <div class="col-md-5 col-sm-5 col-xm-12">
            <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for students..">
            <table id="myTable" border="2" width="30%" cellpadding="2" class="table table-striped table-dark" align="center">
                <tr>
                    <th>Nume</th>
                    <th>Prenume</th>
                    <th>Email</th>
                    <th>Grupa</th>
                    <th>Nota</th>
                </tr>
                <tbody>
                <c:forEach var="nota" items="${note}">
                    <tr>
                        <td>${nota.student.nume}</td>
                        <td>${nota.student.prenume}</td>
                        <td>${nota.student.email}</td>
                        <td>${nota.student.grupa.nrgrupa}</td>
                        <td>${nota.nota}</td>
                        <td>
                            <form action="/changeNotePage/${nota.student.id_student}" method="get" name="updateForm">
                                <button class="btn btn-primary" type="submit" id="update" name="update">Change Note
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</div>
<br/>