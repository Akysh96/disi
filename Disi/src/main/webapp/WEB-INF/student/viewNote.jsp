
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<jsp:include page="/WEB-INF/login/student.jsp"></jsp:include>
<h4>Note List</h4>
<head>
    <script type="text/javascript">

        function validateMyForm()
        {
            if(confirm("Are you sure you want to submit the delete form?"))
            {
                alert("validations passed");
                return true;
            }
            alert("validation false ");
            return false;


        }
    </script>
</head>
<div class="container-fluid ">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xm-12"></div>
        <div class="col-md-5 col-sm-5 col-xm-12">
            <table border="2" width="30%" cellpadding="2" class="table table-striped table-dark" align="center">
                <tr>
                    <th>Materie</th>
                    <th>Nota</th>
                </tr>
                <tbody>
                <c:forEach var="nota" items="${note}">
                    <tr>

                        <td>${nota.materie.denumire}</td>

                        <td>${nota.nota}</td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</div>
<br/>