<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <spring:url value="/resources/css/global.css" var="mainCss"/>



    <link href="${mainCss}" rel="stylesheet"/>
    <title>Welcome</title>
</head>
<body class="bg" style="min-width: 960px; margin: 0 auto;">


<table align="center">
    <tr>


        <td><a class="btn btn-primary" href="/login/profesor">Profesor</a>
        </td>

        <td><a class="btn btn-primary" href="/login/student">Student</a>
        </td>

        <td><a class="btn btn-primary" href="/login/secretariat">Secretariat</a>
        </td>
    </tr>
</table>
</body>
</html>

