package Dao;

import Model.Materie;
import Model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

@Repository
public class MaterieDaoImpl extends DaoGeneric<Materie> implements MaterieDao {

    public MaterieDaoImpl() {
        super(Materie.class);
    }

}
