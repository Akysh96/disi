package Dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class DaoGeneric<T> {

    private Class<T> clazz;



    @Autowired
    protected SessionFactory sessionFactory;


    public DaoGeneric(Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }


    public T findOne(int id) {

        return sessionFactory.getCurrentSession().find(clazz, id);
    }



    public List<T> findAll() {
        List<T> a = null;
        try {
            Session session = sessionFactory.getCurrentSession();

            a = session.createQuery("from " + clazz.getName())
                    .getResultList();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return a;
    }

    public void save(T entity) {

        try{

            Session session = sessionFactory.getCurrentSession();
            session.save(entity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void saveOrUpdate(T entity) {

        try{

            Session session = sessionFactory.getCurrentSession();
            session.saveOrUpdate(entity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void update(T entity) {

        try{

            Session session = sessionFactory.getCurrentSession();
            session.merge(entity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void delete(T entity) {

        try{

            Session session = sessionFactory.getCurrentSession();
            session.remove(entity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public void deleteById(int entityId) {
        T entity = findOne(entityId);
        delete(entity);
    }
}