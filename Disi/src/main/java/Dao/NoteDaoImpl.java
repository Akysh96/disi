package Dao;

import Model.Materie;
import Model.Note;
import Model.Profesor;
import Model.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class NoteDaoImpl extends DaoGeneric<Note> implements NoteDao {

    public NoteDaoImpl() {
        super(Note.class);
    }


    @Override
    public List<Note> findNoteByStudent(Student student) {
        try {
                CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
            CriteriaQuery<Note> query = cb.createQuery(Note.class);
            ParameterExpression<Student> stud = cb.parameter(Student.class, "student");
            Root<Note> planificareMecaniciRoot = query.from(Note.class);
            query.select(planificareMecaniciRoot);
            query.where(cb.equal(planificareMecaniciRoot.get("student"),stud));
            TypedQuery<Note> typedQuery = sessionFactory.getCurrentSession().createQuery(query);
            typedQuery.setParameter("student", student);
            return typedQuery.getResultList();
        }catch (NullPointerException e)
        {
            return null;
        }
    }

    @Override
    public List<Note> findNoteByMaterie(Materie materie) {
        try {
            CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
            CriteriaQuery<Note> query = cb.createQuery(Note.class);
            ParameterExpression<Materie> materie1 = cb.parameter(Materie.class, "materie");
            Root<Note> planificareMecaniciRoot = query.from(Note.class);
            query.select(planificareMecaniciRoot);
            query.where(cb.equal(planificareMecaniciRoot.get("materie"),materie1));
            TypedQuery<Note> typedQuery = sessionFactory.getCurrentSession().createQuery(query);
            typedQuery.setParameter("materie", materie);
            return typedQuery.getResultList();
        }catch (NullPointerException e)
        {
            return null;
        }
    }

}
