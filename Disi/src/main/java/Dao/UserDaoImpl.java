package Dao;

import Model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserDaoImpl extends DaoGeneric<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    public  User findByUsername(String usernameCautat) {

        CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery< User> query = cb.createQuery( User.class);
        ParameterExpression<String> UsernameParametru = cb.parameter(String.class, "username");
        Root< User> userRoot = query.from( User.class);
        query.select(userRoot);
        query.where(cb.equal(userRoot.get("username"), UsernameParametru));
        TypedQuery< User> typedQuery = sessionFactory.getCurrentSession().createQuery(query);
        try {
            return typedQuery.setParameter("username", usernameCautat).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }
}
