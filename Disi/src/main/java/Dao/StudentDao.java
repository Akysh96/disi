package Dao;

import Model.Materie;
import Model.Student;

import java.util.List;

public interface StudentDao {

    void delete(Student student);

    void deleteById(int id);

    void update(Student student);

    Student findOne(int id);

    List<Student> findAll();
    void save(Student student);
    void saveOrUpdate(Student student);
    public  Student findByCnp(String cnp);
    public  Student findByNume(String nume);

}
