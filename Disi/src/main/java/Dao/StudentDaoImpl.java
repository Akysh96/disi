package Dao;

import Model.Materie;
import Model.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

@Repository
public class StudentDaoImpl extends DaoGeneric<Student> implements StudentDao {

    public StudentDaoImpl() {
        super(Student.class);
    }


    public  Student findByCnp(String cnp) {

        CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery< Student> query = cb.createQuery( Student.class);
        ParameterExpression<String> cnpstud = cb.parameter(String.class, "cnp");
        Root< Student> userRoot = query.from( Student.class);
        query.select(userRoot);
        query.where(cb.equal(userRoot.get("cnp"), cnpstud));
        TypedQuery< Student> typedQuery = sessionFactory.getCurrentSession().createQuery(query);
        try {
            return typedQuery.setParameter("cnp", cnp).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }
    public  Student findByNume(String nume) {

        CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery< Student> query = cb.createQuery( Student.class);
        ParameterExpression<String> cnpstud = cb.parameter(String.class, "nume");
        Root< Student> userRoot = query.from( Student.class);
        query.select(userRoot);
        query.where(cb.equal(userRoot.get("nume"), cnpstud));
        TypedQuery< Student> typedQuery = sessionFactory.getCurrentSession().createQuery(query);
        try {
            return typedQuery.setParameter("nume", nume).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }

}
