package Dao;

import Model.Profesor;
import Model.Student;

import java.util.List;

public interface ProfesorDao {

    void delete(Profesor profesor);

    void deleteById(int id);

    void update(Profesor profesor);

    Profesor findOne(int id);

    List<Profesor> findAll();
    void save(Profesor profesor);
    void saveOrUpdate(Profesor profesor);
    Profesor findByNume(String nume);

}
