package Dao;

import Model.User;

import java.util.List;

public interface UserDao {

    void delete(User user);

    void deleteById(int idUser);

    void update(User user);

    User findOne(int id);

    List<User> findAll();
    void save(User user);
    void saveOrUpdate(User user);
    User findByUsername(String usernameCautat);

}
