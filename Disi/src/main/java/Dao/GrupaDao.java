package Dao;

import Model.Grupa;
import Model.Materie;
import Model.Note;
import Model.Student;

import java.util.List;

public interface GrupaDao {

    void delete(Grupa grupa);

    void deleteById(int id);

    void update(Grupa grupa);

    Grupa findOne(int id);

    List<Grupa> findAll();
    void save(Grupa grupa);
    void saveOrUpdate(Grupa grupa);


}
