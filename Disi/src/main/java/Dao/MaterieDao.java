package Dao;

import Model.Materie;
import Model.User;

import java.util.List;

public interface MaterieDao {

    void delete(Materie materie);

    void deleteById(int idmaterie);

    void update(Materie materie);

    Materie findOne(int id);

    List<Materie> findAll();
    void save(Materie materie);
    void saveOrUpdate(Materie materie);

}
