package Dao;

import Model.Profesor;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

@Repository
public class ProfesorDaoImpl extends DaoGeneric<Profesor> implements ProfesorDao {

    public ProfesorDaoImpl() {
        super(Profesor.class);
    }

    public  Profesor findByNume(String nume) {

        CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery< Profesor> query = cb.createQuery( Profesor.class);
        ParameterExpression<String> nume1 = cb.parameter(String.class, "nume");
        Root< Profesor> profesorRoot = query.from( Profesor.class);
        query.select(profesorRoot);
        query.where(cb.equal(profesorRoot.get("nume"), nume1));
        TypedQuery<Profesor> typedQuery = sessionFactory.getCurrentSession().createQuery(query);
        try {
            return typedQuery.setParameter("nume", nume).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }

}
