package Dao;

import Model.Materie;
import Model.Note;
import Model.Profesor;
import Model.Student;

import java.util.List;

public interface NoteDao {

    void delete(Note note);

    void deleteById(int id);

    void update(Note note);

    Note findOne(int id);

    List<Note> findAll();
    void save(Note note);
    void saveOrUpdate(Note note);
    public List<Note> findNoteByStudent(Student student);
    List<Note> findNoteByMaterie(Materie materie);

}
