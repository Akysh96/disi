package Dao;

import Model.Grupa;
import Model.Materie;
import Model.Note;
import Model.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class GrupaDaoImpl extends DaoGeneric<Grupa> implements GrupaDao {

    public GrupaDaoImpl() {
        super(Grupa.class);
    }



}
