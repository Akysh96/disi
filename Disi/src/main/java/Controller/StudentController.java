package Controller;

import Model.Student;
import Model.User;
import Service.StudentService;
import Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/student/all", method = RequestMethod.GET)
    public ModelAndView viewStudents() {
        List<Student> studentList;
        studentList = studentService.findAll();
        return new ModelAndView("view/studentList", "list_students", studentList);
    }

    @RequestMapping(value = "/student/find/{id}", method = RequestMethod.GET)
    public ModelAndView findStudent(@PathVariable int id) {
        Student student = studentService.findOne(id);
        return new ModelAndView("view/student", "one_student", student);
    }

    @RequestMapping(value = "/student/delete/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteStudent(@PathVariable int id) {
        studentService.deleteById(id);
        return new ModelAndView("redirect:/student/all");
    }

    @RequestMapping(value = "/updateStudent/{id}", method = RequestMethod.GET)
    public ModelAndView goToUpdatePage(@PathVariable int id, @ModelAttribute("student") Student student) {

        return new ModelAndView("student/updateStudInfo", "student", studentService.findOne(id));
    }

    @RequestMapping(value = "/updateInfo/{id}", method = RequestMethod.GET)
    public ModelAndView updateInfo(@PathVariable int id, @ModelAttribute("student") Student student) {
        student.setId_student(id);
        studentService.update(student);
        return new ModelAndView("student/viewInformatii", "student", studentService.findOne(id));
    }

    @RequestMapping(value = "/student/update", method = RequestMethod.PUT)
    public ModelAndView updateStudent(@ModelAttribute("student") Student student) {
        studentService.update(student);
        return new ModelAndView("redirect:/student/all");
}

    @RequestMapping(value = "/student/insert", method = RequestMethod.POST)
    public ModelAndView insertStudent(@ModelAttribute("student") Student student) {
        studentService.save(student);
        return new ModelAndView("redirect:/student/all");
    }


    @RequestMapping(value = "/getCnpForInformatii", method = RequestMethod.GET)
    public ModelAndView getCnpForInformatii(HttpSession session, HttpServletRequest request) throws IOException {

        return new ModelAndView("student/getCnpForInformatii");
    }


    @RequestMapping(value = "/viewInformatii", method = RequestMethod.GET)
    public ModelAndView viewInformati(HttpSession session, HttpServletRequest request) throws IOException {

        String numeProf= (String) session.getAttribute("username");
        String[]tokens = numeProf.split("\\.");

        Student student = (studentService.findByNume(tokens[0]));

        return new ModelAndView("student/viewInformatii", "student", student);
    }

}
