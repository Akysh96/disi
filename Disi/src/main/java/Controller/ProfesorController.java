package Controller;

import Model.Profesor;
import Model.Student;
import Service.ProfesorService;
import Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProfesorController {

    @Autowired
    private ProfesorService profesorService;

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/profesor/all", method = RequestMethod.GET)
    public ModelAndView viewProfessors() {
        List<Profesor> profesorList;
        profesorList = profesorService.findAll();
        return new ModelAndView("view/profesorList", "profesor_list", profesorList);
    }

    @RequestMapping(value = "/profesor/find/{id}", method = RequestMethod.GET)
    public ModelAndView findProfesor(@PathVariable int id) {
        Profesor profesor = profesorService.findOne(id);
        return new ModelAndView("view/profesor", "one_profesor", profesor);
    }

    @RequestMapping(value = "/profesor/delete/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteProfesor(@PathVariable int id) {
        profesorService.deleteById(id);
        return new ModelAndView("redirect:/profesor/all");
    }

    @RequestMapping(value = "/profesor/update", method = RequestMethod.PUT)
    public ModelAndView updateProfesor(@ModelAttribute("profesor") Profesor profesor) {
        profesorService.update(profesor);
        return new ModelAndView("redirect:/profesor/all");
    }

    @RequestMapping(value = "/profesor/insert", method = RequestMethod.POST)
    public ModelAndView insertStudent(@ModelAttribute("profesor") Profesor profesor) {
        profesorService.save(profesor);
        return new ModelAndView("redirect:/profesor/all");
    }

    @RequestMapping(value = "/viewStudents", method = RequestMethod.GET)
    public ModelAndView viewStudents(HttpSession session) {
        List<Student> students = new ArrayList<>();
        students = studentService.findAll();
        return new ModelAndView("profesor/viewStudenti", "student", students);
    }

    @RequestMapping(value = "/addNotePage/{id}", method = RequestMethod.GET)
    public ModelAndView viewStudents(@PathVariable int id) {
        return new ModelAndView("profesor/addNote", "student", studentService.findOne(id));
    }
}
