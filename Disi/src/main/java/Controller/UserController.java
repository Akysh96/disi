package Controller;

import Dao.UserDao;
import Model.User;
import Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Controller
public class UserController {

    @Autowired
    UserService userService;


    //pagina de login
    @RequestMapping(value = "/login/{specializare}", method = RequestMethod.GET)
    public ModelAndView showLoginSpecializat(@PathVariable String specializare,HttpSession session) {
        ModelAndView mav = new ModelAndView("/login/login");

        session.removeAttribute("user");

        session.removeAttribute("username");

        session.setAttribute("type",specializare);

        return mav;
    }

    @RequestMapping("/loginProcess")
    public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

        String inputUsername = request.getParameter("username");
        String inputPassword = request.getParameter("password");

        session.setAttribute("username",inputUsername);

        User user = userService.findByUsername(inputUsername);

        if( user != null ) {
            System.out.println("Userul=" + user.toString());
            String userPassword = user.getPassword();
            String userType = user.getTip_user();

            //System.out.println("User pass = " + userPassword);
            //System.out.println("User pass = " + userType);

            String message = "";
            if (inputPassword.equals(userPassword)) {
                message = "Welcome " + inputUsername;

                if (userType.equalsIgnoreCase("student")) {
                    return new ModelAndView("login/student", "message", message);
                } else if (userType.equalsIgnoreCase("profesor")) {
                    return new ModelAndView("login/profesor", "message", message);
                }else if (userType.equalsIgnoreCase("secretariat")) {
                    return new ModelAndView("login/secretariat", "message", message);
                }
                return new ModelAndView("/login/errorPage", "message", "Invalid UserType");

            }
        }

        return new ModelAndView("/login/login", "message", "Invalid username or password");
    }


    @RequestMapping("/formUser")
    public ModelAndView showform(HttpSession session) {

        ModelAndView mav = new ModelAndView("login/register");
        session.getAttribute("Mesaj");
        mav.addObject("message", session.getAttribute("Mesaj"));
        mav.addObject("command", new User());
        session.removeAttribute("Mesaj");
        return mav;
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ModelAndView save(HttpSession session, HttpServletRequest request) throws IOException {

        User user = new User();
        user.setUsername(request.getParameter("usernmae"));
        user.setPassword(request.getParameter("password"));
        user.setTip_user(request.getParameter("tip_user"));

        userService.save(user);

        return new ModelAndView("views/index");
    }


    @RequestMapping(value = "/viewUser", method = RequestMethod.GET)
    public ModelAndView viewUser(HttpSession session) {
        List<User> userList;
        userList = userService.findAll();

        return new ModelAndView("view/userList", "userList", userList);

    }

    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable int id, HttpSession session) {
        userService.deleteById(id);
        return new ModelAndView("redirect:/viewUser");

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpSession session) {
        ModelAndView mav = new ModelAndView("/login/login");
        session.removeAttribute("user");
        session.removeAttribute("firstname");


        return mav;
    }

}
