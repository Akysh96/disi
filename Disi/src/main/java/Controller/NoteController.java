package Controller;

import Model.Materie;
import Model.Note;
import Model.Student;
import Service.MaterieService;
import Service.NoteService;
import Service.ProfesorService;
import Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Controller
public class NoteController {
    @Autowired
    private NoteService noteService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private ProfesorService profesorService;
    @Autowired
    private MaterieService materieService;

    @RequestMapping(value = "/gerade/all", method = RequestMethod.GET)
    public ModelAndView viewGrades() {
        List<Note> noteList;
        noteList = noteService.findAll();
        return new ModelAndView("view/grades", "noteList", noteList);
    }

    @RequestMapping(value = "/viewNote", method = RequestMethod.GET)
    public ModelAndView viewNote(HttpSession session, HttpServletRequest request) throws IOException {

        List<Note> note = new ArrayList<>();
        String numeProf = (String) session.getAttribute("username");
        String[] tokens = numeProf.split("\\.");

        Student student = (studentService.findByNume(tokens[0]));
        note = noteService.findNoteByStudent(student);

        return new ModelAndView("student/viewNote", "note", note);
    }

    @RequestMapping(value = "/viewNoteProf", method = RequestMethod.GET)
    public ModelAndView viewNoteProf(HttpSession session, HttpServletRequest request) throws IOException {

        List<Note> note = new ArrayList<>();
        String numeProf = (String) session.getAttribute("username");
        String[] tokens = numeProf.split("\\.");
        Materie materie = materieService.findOne(profesorService.findByNume((tokens[0])).getMaterie());
        note = noteService.findNoteByMaterie(materie);

        return new ModelAndView("profesor/viewNote", "note", note);
    }

    @RequestMapping(value = "/changeNotePage/{id}", method = RequestMethod.GET)
    public ModelAndView changeNoteProf(@PathVariable int id) throws IOException {

        return new ModelAndView("profesor/changeNote", "student", studentService.findOne(id));
    }

    @RequestMapping(value = "/changeNote/{id}", method = RequestMethod.GET)
    public ModelAndView changeNote(@PathVariable int id, HttpSession session, HttpServletRequest request) throws IOException {
        List<Note> note = new ArrayList<>();
//        Materie materie = materieService.findOne(profesorService.findByNume((String) session.getAttribute("username")).getMaterie());
        String numeProf = (String) session.getAttribute("username");
        String[] tokens = numeProf.split("\\.");
        Materie materie = materieService.findOne(profesorService.findByNume((tokens[0])).getMaterie());
        note = noteService.findNoteByMaterie(materie);
        Note nota1 = null;


        for (Note nota : note) {
            if (nota.getStudent().getId_student() == id) {
                nota1 = nota;
                break;
            }
        }

        nota1.setNota(request.getParameter("nota"));
        noteService.update(nota1);
        noteService.notifyStudentGradeUpdate(nota1);

        note = noteService.findNoteByMaterie(materie);
        return new ModelAndView("profesor/viewNote", "note", note);
    }

    @RequestMapping(value = "/getCnpForNote", method = RequestMethod.GET)
    public ModelAndView getCnpForNote(HttpSession session, HttpServletRequest request) throws IOException {

        return new ModelAndView("student/getCnpForNote");
    }

    @RequestMapping(value = "/addNote/{id}", method = RequestMethod.POST)
    public ModelAndView viewNote(@PathVariable int id, HttpSession session, HttpServletRequest request) throws IOException {
        Note note = new Note();
        note.setNota(request.getParameter("nota"));


        note.setStudent(studentService.findOne(id));
//        Materie materie = materieService.findOne(profesorService.findByNume((String) session.getAttribute("username")).getMaterie());
        String numeProf = (String) session.getAttribute("username");
        String[] tokens = numeProf.split("\\.");
        Materie materie = materieService.findOne(profesorService.findByNume((tokens[0])).getMaterie());
        note.setMaterie(materie);
        noteService.save(note);
        noteService.notifyStudentGradeUpdate(note);


        return new ModelAndView("profesor/viewStudenti", "student", studentService.findAll());
    }

    @RequestMapping(value = "/grade/find/{id}", method = RequestMethod.GET)
    public ModelAndView findGrade(@PathVariable int id) {
        Note nota = noteService.findOne(id);
        return new ModelAndView("view/grade", "one_grade", nota);
    }

    @RequestMapping(value = "/grade/delete/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteGrade(@PathVariable int id) {
        noteService.deleteById(id);
        return new ModelAndView("redirect:/gerade/all");
    }

    @RequestMapping(value = "/grade/update", method = RequestMethod.PUT)
    public ModelAndView updateGrade(@ModelAttribute("note") Note note) {
        noteService.update(note);
        return new ModelAndView("redirect:/gerade/all");
    }

    @RequestMapping(value = "/grade/insert", method = RequestMethod.POST)
    public ModelAndView insertStudent(@ModelAttribute("note") Note note) {
        noteService.save(note);
        return new ModelAndView("redirect:/gerade/all");
    }

}
