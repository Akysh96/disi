package Controller;

import Model.Grupa;
import Model.Profesor;
import Model.Student;
import Model.User;
import Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.portlet.MockActionRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by akysh_000 on 5/19/2019.
 */
@Controller
public class SecretariatController {
    private static final String STUD_PREFIX = "student";
    private static final String PROF_PREFIX = "profesor";

    @Autowired
    UserService userService;
    @Autowired
    StudentService studentService;
    @Autowired
    ProfesorService profesorService;
    @Autowired
    GrupaService grupaService;
    @Autowired
    MaterieService materieService;

    @RequestMapping(value = "/enrollStudent", method = RequestMethod.POST)
    public ModelAndView enrollStudent(HttpSession session, Student student) throws IOException {
        User user = new User();
        user.setUsername(student.getNume() + "." + student.getPrenume());
        user.setPassword(student.getNume());
        user.setTip_user(STUD_PREFIX);
        userService.save(user);
        studentService.save(student);
        return new ModelAndView("secretariat/enrollStudent");
    }

    @RequestMapping(value = "/viewEnrollStudent", method = RequestMethod.GET)
    public ModelAndView viewEnrollStudent(HttpSession session) throws IOException {
        return new ModelAndView("secretariat/enrollStudent");
    }

    @RequestMapping(value = "/addProfessor", method = RequestMethod.POST)
    public ModelAndView addProfessor(HttpSession session, HttpServletRequest request, Profesor profesor) throws IOException {
        profesor.setMaterie(Integer.valueOf(request.getParameter("materia")));
        User user = new User();
        user.setUsername(profesor.getNume() + "." + profesor.getPrenume());
        user.setPassword(profesor.getNume());
        user.setTip_user(PROF_PREFIX);
        userService.save(user);
        profesorService.save(profesor);
        return new ModelAndView("redirect:/viewAddProfessor");
    }

    @RequestMapping(value = "/viewAddProfessor", method = RequestMethod.GET)
    public ModelAndView viewAddProfessor(HttpSession session) throws IOException {
        return new ModelAndView("secretariat/addProfessor", "materie", materieService.findAll());
    }

    @RequestMapping(value = "/viewStudentiInfo", method = RequestMethod.GET)
    public ModelAndView viewStudentInfo(HttpSession session)throws IOException{
        List<Student> students = new ArrayList<>();
        students = studentService.findAll();
        return new ModelAndView("secretariat/viewStudenti", "student", students);
    }

    @RequestMapping(value = "/viewAddToGroup/{id}", method = RequestMethod.GET)
    public ModelAndView viewAddToGroup(@PathVariable int id, HttpSession session)throws IOException{
        session.setAttribute("id_stud", id);
        return new ModelAndView("secretariat/addToGroup");
    }

    @RequestMapping(value = "/addToGroup", method = RequestMethod.POST)
    public ModelAndView addToGroup(HttpSession session, HttpServletRequest request)throws IOException{
        int grupaNr = Integer.valueOf(request.getParameter("group"));
        Grupa grupa = new Grupa();

        if( grupaService.findOne(grupaNr) == null){
            grupa.setNrgrupa(String.valueOf(grupaNr));
            grupaService.save(grupa);
        }
        else
        {
            grupa= grupaService.findOne(grupaNr);
        }
        int studId = (int) session.getAttribute("id_stud");
        Student student = studentService.findOne(studId);
        student.setGrupa(grupa);
        studentService.update(student);
        List<Student> students = studentService.findAll();
        return new ModelAndView("secretariat/viewStudenti", "student", students);
    }


    @RequestMapping(value = "/viewChangeInfo/{id}", method = RequestMethod.GET)
    public ModelAndView viewChangeInfo(@PathVariable int id, HttpSession session)throws IOException{
        session.setAttribute("stud_id", id);
        return new ModelAndView("secretariat/changeStudentInfo", "student", studentService.findOne(id));
    }


    @RequestMapping(value = "/changeInfoStudent", method = RequestMethod.POST)
    public ModelAndView changeInfoStudent(HttpSession session, Student student) throws IOException {
        int studId = (int) session.getAttribute("stud_id");
        Student student1 = studentService.findOne(studId);
        student.setId_student(studId);
        student.setGrupa(student1.getGrupa());
        studentService.update(student);
        List<Student> students = studentService.findAll();
        return new ModelAndView("secretariat/viewStudenti", "student", students);
    }
}
