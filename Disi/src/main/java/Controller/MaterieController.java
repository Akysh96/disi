package Controller;

import Model.Materie;
import Model.Note;
import Service.MaterieService;
import Service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MaterieController {
    @Autowired
    private MaterieService materieService;

    @RequestMapping(value = "/materie/all", method = RequestMethod.GET)
    public ModelAndView viewMaterii() {
        List<Materie> materieList;
        materieList = materieService.findAll();
        return new ModelAndView("view/materi", "materieList", materieList);
    }

    @RequestMapping(value = "/materie/find/{id}", method = RequestMethod.GET)
    public ModelAndView findMaterie(@PathVariable int id) {
        Materie materie = materieService.findOne(id);
        return new ModelAndView("view/materie", "materie", materie);
    }

    @RequestMapping(value = "/materie/delete/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteMaterie(@PathVariable int id) {
        materieService.deleteById(id);
        return new ModelAndView("redirect:/materie/all");
    }

    @RequestMapping(value = "/materie/update", method = RequestMethod.PUT)
    public ModelAndView updateMaterie(@ModelAttribute("materie") Materie materie) {
        materieService.update(materie);
        return new ModelAndView("redirect:/materie/all");
    }

    @RequestMapping(value = "/materie/insert", method = RequestMethod.POST)
    public ModelAndView insertMaterie(@ModelAttribute("materie") Materie materie) {
        materieService.save(materie);
        return new ModelAndView("redirect:/materie/all");
    }
}
