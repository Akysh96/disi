package Controller;

import antlr.collections.List;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main {


    public static void main(String[] args){
        ArrayList<Integer> arrayList_1 = new ArrayList<>();
        ArrayList<Integer> arrayList_2 = new ArrayList<>();
        arrayList_1.add(25);
        arrayList_1.add(5);
        arrayList_1.add(7);
        arrayList_1.add(1);
        arrayList_1.add(123);


        arrayList_2.add(52);
        arrayList_2.add(5);
        arrayList_2.add(7);
        arrayList_2.add(1);

        arrayList_2.add(125);

        //arrayList_1.addAll(arrayList_2);

        for( Integer integer : arrayList_2 ){
            if( !arrayList_1.contains(integer) ){
                arrayList_1.add(integer);
            }
        }

        Collections.sort(arrayList_1, Collections.reverseOrder() );

        System.out.println(arrayList_1);
        //arrayList_1.clear();


        System.out.println( Collections.max(arrayList_1) );

        Collections.replaceAll(arrayList_1,  Collections.max(arrayList_1),99999);
        System.out.println(arrayList_1);

    }
}
