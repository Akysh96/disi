package Service;


import Model.Materie;
import Model.User;

import java.util.List;

public interface MaterieService {

    void delete(Materie user);

    void deleteById(int idUSer);

    void update(Materie user);

    Materie  findOne(int id);

    List<Materie > findAll();

    void save(Materie user);
}
