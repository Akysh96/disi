package Service;


import Model.Grupa;
import Model.Materie;
import Model.Note;
import Model.Student;

import java.util.List;

public interface GrupaService {

    void delete(Grupa grupa);

    void deleteById(int id);

    void update(Grupa grupa);

    Grupa findOne(int id);

    List<Grupa> findAll();
    void save(Grupa grupa);

}
