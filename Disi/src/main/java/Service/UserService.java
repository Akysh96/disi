package Service;


import Model.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    void delete(User user);

    void deleteById(int idUSer);

    void update(User user);

    User  findOne(int id);

    List<User > findAll();

    void save(User user);
    User findByUsername(String usernameCautat);
}
