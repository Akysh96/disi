package Service;

import Dao.ProfesorDao;
import Model.Profesor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProfesorServiceImpl implements ProfesorService {

    @Autowired
    ProfesorDao profesorDao;

    @Transactional
    public void save(Profesor profesor) {
        profesorDao.save(profesor);
    }


    @Transactional
    public Profesor findOne(int id) {
        return  profesorDao.findOne(id);
    }
    @Transactional
    public void delete(Profesor profesor) {
        profesorDao.delete(profesor);
    }

    @Transactional
    public void update(Profesor profesor) {
        profesorDao.update(profesor);
    }

    @Transactional
    public void deleteById(int id) {
        profesorDao.deleteById(id);
    }

    @Transactional
    public List<Profesor> findAll() {
        return profesorDao.findAll();
    }

    @Transactional
    public Profesor findByNume(String nume) {
        return profesorDao.findByNume(nume);
    }


}