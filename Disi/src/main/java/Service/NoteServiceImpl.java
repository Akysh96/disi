package Service;

import Dao.NoteDao;
import MailService.MailService;
import Model.Materie;
import Model.Note;
import Model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    NoteDao noteDao;

    MailService mailService;


    @Transactional
    public void save(Note note) {
        noteDao.save(note);
    }

    @Transactional
    public Note findOne(int id) {
        return noteDao.findOne(id);
    }

    @Transactional
    public void delete(Note note) {
        noteDao.delete(note);
    }

    @Transactional
    public void update(Note note) {
        noteDao.update(note);
    }

    @Transactional
    public void deleteById(int id) {
        noteDao.deleteById(id);
    }

    @Transactional
    public List<Note> findAll() {
        return noteDao.findAll();
    }

    @Transactional
    public List<Note> findNoteByStudent(Student student) {
        return noteDao.findNoteByStudent(student);
    }

    @Transactional
    public List<Note> findNoteByMaterie(Materie materie) {
        return noteDao.findNoteByMaterie(materie);
    }

    @Transactional
    public void notifyStudentGradeUpdate(Note nota) {
        if (mailService == null) {
            mailService = new MailService("plesa.dummy@gmail.com", "Aceastaesteparola");
        }

        Student student = nota.getStudent();
        Materie materie = nota.getMaterie();
        String nouaNota = nota.getNota();
        String emailStudent = student.getEmail();

        String emailSubject = "Nota la materia " + materie.getDenumire();
        String emailContent = "Salutare " + student.getNume() + " " + student.getPrenume() + ", \n\n " + "Nota la materia: " + materie.getDenumire() + " a fost actualizata/adaugata:" + nouaNota + " \n\n " + "O zi buna";
        mailService.sendMail(emailStudent, emailSubject, emailContent);
    }
}