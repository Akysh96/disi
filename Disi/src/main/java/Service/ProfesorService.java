package Service;


import Model.Profesor;
import Model.User;

import java.util.List;

public interface ProfesorService {

    void delete( Profesor profesor);

    void deleteById(int idUSer);

    void update( Profesor profesor);

    Profesor  findOne(int id);

    List<Profesor> findAll();

    void save( Profesor profesor);
    Profesor findByNume(String nume);
}
