package Service;


import Model.Materie;
import Model.Note;
import Model.Student;
import Model.User;

import java.util.List;

public interface NoteService {

    void delete(Note note);

    void deleteById(int id);

    void update(Note note);

    Note  findOne(int id);

    List<Note > findAll();

    void save(Note note);

    public List<Note> findNoteByStudent(Student student);
    List<Note> findNoteByMaterie(Materie materie);

    void notifyStudentGradeUpdate(Note nota);
}
