package Service;

import Dao.MaterieDao;
import Dao.NoteDao;
import Dao.UserDao;
import Model.Materie;
import Model.Note;
import Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MaterieServiceImpl implements MaterieService {

    @Autowired
    MaterieDao materieDao;

    @Transactional
    public void save(Materie materie) {
        materieDao.save(materie);
    }


    @Transactional
    public Materie findOne(int id) {
        return  materieDao.findOne(id);
    }
    @Transactional
    public void delete(Materie materie) {
        materieDao.delete(materie);
    }

    @Transactional
    public void update(Materie materie) {
        materieDao.update(materie);
    }

    @Transactional
    public void deleteById(int id) {
        materieDao.deleteById(id);
    }

    @Transactional
    public List<Materie> findAll() {
        return materieDao.findAll();
    }


}