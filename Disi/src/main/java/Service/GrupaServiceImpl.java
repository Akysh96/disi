package Service;

import Dao.GrupaDao;
import Model.Grupa;
import Model.Materie;
import Model.Note;
import Model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GrupaServiceImpl implements GrupaService {

    @Autowired
    GrupaDao grupaDao;

    @Transactional
    public void save(Grupa note) {
        grupaDao.save(note);
    }

    @Transactional
    public Grupa findOne(int id) {
        return  grupaDao.findOne(id);
    }

    @Transactional
    public void delete(Grupa note) {
        grupaDao.delete(note);
    }

    @Transactional
    public void update(Grupa note) {
        grupaDao.update(note);
    }

    @Transactional
    public void deleteById(int id) {
        grupaDao.deleteById(id);
    }

    @Transactional
    public List<Grupa> findAll() {
        return grupaDao.findAll();
    }


}