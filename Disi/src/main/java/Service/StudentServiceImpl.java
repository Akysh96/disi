package Service;

import Dao.StudentDao;
import Model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentDao studentDao;

    @Transactional
    public void save(Student student) {
        studentDao.save(student);
    }


    @Transactional
    public Student findOne(int id) {
        return  studentDao.findOne(id);
    }
    @Transactional
    public void delete(Student student) {
        studentDao.delete(student);
    }

    @Transactional
    public void update(Student student) {
        studentDao.update(student);
    }

    @Transactional
    public void deleteById(int id) {
        studentDao.deleteById(id);
    }

    @Transactional
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    @Transactional
    public  Student findByCnp(String cnp){
        return studentDao.findByCnp(cnp);
    }

    @Transactional
    public  Student findByNume(String nume){
        return studentDao.findByNume(nume);
    }


}