package Service;


import Model.Student;
import Model.User;

import java.util.List;

public interface StudentService {

    void delete(Student student);

    void deleteById(int id);

    void update(Student student);

    Student  findOne(int id);

    List<Student > findAll();

    void save(Student student);
    Student findByCnp(String cnp);
      Student findByNume(String nume);
}
