package Service;

import Dao.UserDao;
import Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Transactional
    public void save(User user) {
        userDao.save(user);
    }


    @Transactional
    public User findOne(int idUser) {
        return  userDao.findOne(idUser);
    }
    @Transactional
    public void delete(User user) {
        userDao.delete(user);
    }

    @Transactional
    public void update(User user) {
        userDao.update(user);
    }

    @Transactional
    public void deleteById(int idUser) {
        userDao.deleteById(idUser);
    }

    @Transactional
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Transactional
    public User findByUsername(String usernameCautat) {
        return userDao.findByUsername(usernameCautat);
    }

}