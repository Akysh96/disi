package Model;


import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.util.List;

@Entity
@OptimisticLocking(type = OptimisticLockType.ALL)
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "profesor")
public class Profesor {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_profesor", updatable = false, nullable = false)
    private int id_profesor;
    @Column(name = "nume")
    private String nume;
    @Column(name = "prenume")
    private String prenume;
 /*   @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )*/
    @Column(name = "id_materie")
    private int id_materie;

    public Profesor() {
    }

    public int getId_profesor() {
        return id_profesor;
    }

    public void setId_profesor(int id_profesor) {
        this.id_profesor = id_profesor;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public int getMaterie() {
        return id_materie;
    }

    public void setMaterie(int id_materie) { this.id_materie = id_materie;
    }
}
