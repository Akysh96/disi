package Model;


import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.util.List;

@Entity
@OptimisticLocking(type = OptimisticLockType.ALL)
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "grupa")
public class Grupa {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_grupa", updatable = false, nullable = false)
    private int id_grupa;
    @Column(name = "nrgrupa")
    private String nrgrupa;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_student")
    private List<Student> student;


    public Grupa() {
    }

    public int getId_grupa() {
        return id_grupa;
    }

    public void setId_grupa(int id_grupa) {
        this.id_grupa = id_grupa;
    }

    public String getNrgrupa() {
        return nrgrupa;
    }

    public void setNrgrupa(String nrgrupa) {
        this.nrgrupa = nrgrupa;
    }

    public List<Student> getStudent() {
        return student;
    }

    public void setStudent(List<Student> student) {
        this.student = student;
    }
}
