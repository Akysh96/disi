-- create database disi;
-- drop database service;

drop table if exists user;
drop table if exists note;
drop table if exists profesor;
drop table if exists materie;
drop table if exists student;
drop table if exists grupa;



create table user(

	`id_user` int(11) not null AUTO_INCREMENT,
	
	`username` varchar(20) ,
  
	`password` varchar(20) ,
  
	`tip_user` varchar(20),
    
	PRIMARY KEY (`id_user`),
	UNIQUE KEY `id_user` (`id_user`)

);
 
 
 
create table grupa(

	`id_grupa` int(11) not null AUTO_INCREMENT,
	
	`nrgrupa` int(20) ,

        
	PRIMARY KEY (`id_grupa`),
	UNIQUE KEY `id_grupa` (`id_grupa`)

);
 
 
 create table student(

	`id_student` int(11) not null AUTO_INCREMENT,
	
	`nume` varchar(20) ,
  
	`prenume` varchar(20) ,
  
	`cnp` varchar(20) ,
    
	`email` varchar(40) ,
    
	`id_grupa` int(40) ,
    
    
    
	PRIMARY KEY (`id_student`),
	UNIQUE KEY `id_student` (`id_student`)

);

  alter table student
	add constraint fk_nr_grupa
	foreign key (id_grupa)
	references grupa(id_grupa)
	on update cascade
	on delete cascade;


create table materie(

	`id_materie` int(11) not null AUTO_INCREMENT,
	
	`denumire` varchar(20) ,

        
	PRIMARY KEY (`id_materie`),
	UNIQUE KEY `id_materie` (`id_materie`)

);


 create table profesor(

	`id_profesor` int(11) not null AUTO_INCREMENT,
	
	`nume` varchar(20) ,
  
	`prenume` varchar(20) ,
  
	`id_materie` int(20) ,

        
	PRIMARY KEY (`id_profesor`),
	UNIQUE KEY `id_profesor` (`id_profesor`)

);
  alter table profesor
	add constraint fk_materie
	foreign key (id_materie)
	references materie(id_materie)
	on update cascade
	on delete cascade;
    


create table note(

	`id_note` int(11) not null AUTO_INCREMENT,
	
	`nota` varchar(20) ,
    
    	`id_materie` int(20) ,
    	`id_student` int(20) ,

        
	PRIMARY KEY (`id_note`),
	UNIQUE KEY `id_note` (`id_note`)

);


  alter table note
	add constraint fk_note_materie
	foreign key (id_materie)
	references materie(id_materie)
	on update cascade
	on delete cascade;
    
    
      alter table note
	add constraint fk_note_student
	foreign key (id_student)
	references student(id_student)
	on update cascade
	on delete cascade;
    


INSERT INTO `disi`.`grupa` (`id_grupa`, `nrgrupa`) VALUES ('1', '1');
INSERT INTO `disi`.`grupa` (`id_grupa`, `nrgrupa`) VALUES ('2', '2');
INSERT INTO `disi`.`grupa` (`id_grupa`, `nrgrupa`) VALUES ('3', '3');


INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('1', 'SCTI');
INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('2', 'DISI');
INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('3', 'ARC');
INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('4', 'SSA');
INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('5', 'RC');
INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('6', 'PSI');
INSERT INTO `disi`.`materie` (`id_materie`, `denumire`) VALUES ('7', 'SSC');


INSERT INTO `disi`.`profesor` (`id_profesor`, `nume`, `prenume`, `id_materie`) VALUES ('1', 'Pop', 'Vasile', '1');
INSERT INTO `disi`.`profesor` (`id_profesor`, `nume`, `prenume`, `id_materie`) VALUES ('2', 'Virgil', 'Iantu', '2');
INSERT INTO `disi`.`profesor` (`id_profesor`, `nume`, `prenume`, `id_materie`) VALUES ('3', 'Adi', 'Minim', '3');
INSERT INTO `disi`.`profesor` (`id_profesor`, `nume`, `prenume`, `id_materie`) VALUES ('4', 'Florin', 'Salomie', '4');
INSERT INTO `disi`.`profesor` (`id_profesor`, `nume`, `prenume`, `id_materie`) VALUES ('5', 'Nea', 'Ilie', '6');
INSERT INTO `disi`.`profesor` (`id_profesor`, `nume`, `prenume`, `id_materie`) VALUES ('6', 'Bogdan', 'Iancu', '5');


INSERT INTO `disi`.`student` (`id_student`, `nume`, `prenume`, `cnp`, `email`, `id_grupa`) VALUES ('1', 'Savan', 'Catalin', '1234567890123', 'sav.cat@student.cluj.ro', '1');
INSERT INTO `disi`.`student` (`id_student`, `nume`, `prenume`, `cnp`, `email`, `id_grupa`) VALUES ('2', 'Plesa', 'Gabriel', '1234567890124', 'ple.gab@student.cluj.ro', '1');
INSERT INTO `disi`.`student` (`id_student`, `nume`, `prenume`, `cnp`, `email`, `id_grupa`) VALUES ('3', 'Akysh', 'Akysh', '1234567890125', 'aky.aky@student.cluj.ro', '2');
INSERT INTO `disi`.`student` (`id_student`, `nume`, `prenume`, `cnp`, `email`, `id_grupa`) VALUES ('4', 'Vigu', 'Viorel', '1234567890126', 'vig.vio@student.cluj.ro', '2');


INSERT INTO `disi`.`note` (`id_note`, `nota`, `id_materie`, `id_student`) VALUES ('1', '5', '1', '1');
INSERT INTO `disi`.`note` (`id_note`, `nota`, `id_materie`, `id_student`) VALUES ('2', '5', '2', '2');
INSERT INTO `disi`.`note` (`id_note`, `nota`, `id_materie`, `id_student`) VALUES ('3', '5', '3', '2');
INSERT INTO `disi`.`note` (`id_note`, `nota`, `id_materie`, `id_student`) VALUES ('4', '5', '4', '1');
INSERT INTO `disi`.`note` (`id_note`, `nota`, `id_materie`, `id_student`) VALUES ('5', '5', '5', '3');


INSERT INTO `disi`.`user` (`id_user`, `username`, `password`, `tip_user`) VALUES ('1', 'secretariat', 'secretariat', 'secretariat');
INSERT INTO `disi`.`user` (`id_user`, `username`, `password`, `tip_user`) VALUES ('2', 'pop.vasile', 'pop', 'profesor');
INSERT INTO `disi`.`user` (`id_user`, `username`, `password`, `tip_user`) VALUES ('3', 'savan.catalin', 'savan', 'student');
INSERT INTO `disi`.`user` (`id_user`, `username`, `password`, `tip_user`) VALUES ('4', 'plesa.gabriel', 'plesa', 'student');
INSERT INTO `disi`.`user` (`id_user`, `username`, `password`, `tip_user`) VALUES ('5', 'virgil.iantu', 'virgil', 'profesor');
INSERT INTO `disi`.`user` (`id_user`, `username`, `password`, `tip_user`) VALUES ('6', 'akysh.akysh', 'akysh', 'student');




 
 